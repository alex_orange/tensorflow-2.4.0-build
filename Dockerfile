FROM ubuntu:20.04

RUN apt-get update && \
    apt-get install -y python3-dev python3-pip python3-venv curl gnupg

RUN curl https://bazel.build/bazel-release.pub.gpg | apt-key add -
RUN echo "deb [arch=amd64] https://storage.googleapis.com/bazel-apt stable jdk1.8" > /etc/apt/sources.list.d/bazel.list
RUN apt-get update && apt-get install -y bazel-3.1.0
